'use strict';

import express from 'express';
import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import Html from '../../containers/Html';
import App from '../../containers/App';

const router = express.Router();

router.get('*', (req, res) => {
    /*
        We could use react router to handle routes matching
        but there are only 2 routes (index and 404) so it's
        not worth it.
    */

    if(req.originalUrl !== '/'){
        return res.status(404).send(generateHTML(req.originalUrl, 'not-found'));
    }

    res.send(generateHTML(req.originalUrl, 'home'));
});

const generateHTML = (path, bodyClass) => renderToStaticMarkup(
    <Html bodyClass={ bodyClass }>
    <App path={ path } />
    </Html>
);

export default router;
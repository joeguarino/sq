'use strict';

const PRODUCTION = process.argv.indexOf('-p') !== -1;
const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const ExtendedDefinePlugin = require('extended-define-webpack-plugin');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const chalk = require('chalk');

let plugins = [
    new ExtractTextPlugin({
        filename: '../css/app.min.css',
        allChunks: true
    }),
    new OptimizeCssAssetsPlugin({
        cssProcessorOptions: {
            discardComments: {
                removeAll: true
            }
        }
    }),
    new ProgressBarPlugin({
        format: '  build [:bar] ' + chalk.green.bold(':percent') + ' (:elapsed seconds)',
        clear: false
    }),
    new webpack.optimize.UglifyJsPlugin({
        comments: false,
    }),
    new ExtendedDefinePlugin({
        "process.env": process.env
    }),
    new ExtendedDefinePlugin({
        'process.browser': true
    }),
    new webpack.optimize.AggressiveMergingPlugin()
];


const config = {
    entry: {
        'app': [path.join(__dirname, 'src', 'js', 'client.js')]
    },
    output: {
        path: path.join(__dirname, 'public', 'js'),
        publicPath: '/',
        filename: '[name].min.js'
    },
    watchOptions: {
        poll: true
    },
    devtool: !PRODUCTION ? 'inline-sourcemap' : false,
    resolve: {
        extensions: ['.js', '.jsx', '.scss', '.css'],
        modules: ['node_modules']
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                use: ['babel-loader'],
                exclude: [/node_modules/]
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: ['css-loader', 'sass-loader']
                })
            }
        ]
    },
    node: {
        console: !PRODUCTION,
        fs: 'empty',
        net: 'empty',
        tls: 'empty',
        child_process: 'empty'
    }
};

config.plugins = plugins;

module.exports = config;
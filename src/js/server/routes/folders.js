'use strict';

import express from 'express';
const router = express.Router();

router.use('/public', express.static('./public'));

export default router;
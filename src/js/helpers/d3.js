'use strict';

import * as d3 from 'd3';
import { screenWidth } from './viewport';

const timeFormats = ['%Y', '%Y-%m', '%Y-%m-%d'];
const timeFormat = (zoom) => timeFormats[zoom];
const parseOriginalDate = d3.timeParse('%Y-%m-%d');

export const formatData = (data, zoom, multiplier = 0) => {
    // Given an array of instruments we format its response by aggregating itss values
    return data.map((entry) => {
        return {
            id: entry.instrumentId,
            values: formatEntry(entry.timeSeries.entries, zoom, multiplier)
        };
    });
};

export const formatEntry = (data, zoom = 0, multiplier = 0) => {
    /*
        Based on the zoom level we format everything according to this pattern:
        0. Group by years
        1. Group by months
        2. Group by days
    */

    let entry = JSON.parse(JSON.stringify(data)); // clone to avoid updating original data
    let parseDate = d3.timeParse(timeFormat(zoom));

    if(zoom === 0){
        // First we group the values by year-month
        let pairs = entry.reduce((v, d) => {
            let year = parseOriginalDate(d.d).getFullYear();
            if(!!year){
                if(!v[year]){
                    v[year] = [];
                }
                v[year].push(d.v * (multiplier + 1));
            }
            return v;
        }, {});

        // Then for each year we calculate the AVG and we store everything into formatted
        return Object.keys(pairs).reduce((v, y) => {
            let avg = [];
            if(!!pairs[y] && pairs[y].length > 0){
                avg = pairs[y].reduce((a, b) => a + b, 0) / pairs[y].length;
            }
            v.push({
                date: parseDate(y),
                value: avg
            });
            return v;
        }, []);
    }
    else if(zoom === 1){
        // First we group the values by year-month
        let pairs = entry.reduce((v, d) => {
            let date = parseOriginalDate(d.d);
            let year = date.getFullYear();
            let month = date.getMonth().toString().padStart(2, '0');
            if(!!year && !!month){
                if(!v[`${year}-${month}`]){
                    v[`${year}-${month}`] = [];
                }
                v[`${year}-${month}`].push(d.v * (multiplier + 1));
            }
            return v;
        }, {});

        // Then for each year-month we calculate the AVG and we store everything into formatted
        return Object.keys(pairs).reduce((v, ym) => {
            let avg = [];
            if(!!pairs[ym] && pairs[ym].length > 0){
                avg = pairs[ym].reduce((a, b) => a + b, 0) / pairs[ym].length;
            }
            v.push({
                date: parseDate(ym),
                value: avg
            });
            return v;
        }, []);
    }
    else if(zoom === 2){
        entry.forEach((d) => {
            d.date = parseDate(d.d);
            d.value = +d.v * (multiplier + 1);
        });
        return entry;
    }

    return [];
};

export const buildGraph = (selector, data, zoom = 0, multiplier = 0) =>{
    if(!data || (data || []).length === 0){
        return;
    }

    let wrapperMargin = 20;

    // Define margins and size
    let margin = {top: 40, right: 40, bottom: 40, left: 60},
        width = Math.min(screenWidth(), 1000) - margin.left - margin.right - (wrapperMargin * 2),
        height = 360 - margin.top - margin.bottom;

    // X-Axis
    let x = d3.scaleTime().rangeRound([0, width]);

    // Y-Axis
    let y = d3.scaleLinear().rangeRound([height, 0]);

    // Select SVG from DOM
    let svg = d3.select(selector);

    // Set size to SVG
    svg.attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');


    // Create X-Axis and its grid
    let xAxis = svg.append('g');
    let gridX = svg.append('g');

    // Create Y-Axis and its grid
    let yAxis = svg.append('g');
    let gridY = svg.append('g');

    // Define the line
    let line = d3.line()
        .x((d) => x(d.date) + margin.left)
        .y((d) => y(d.value) + margin.top);

    let entries = formatData(data, zoom, multiplier);

    // Scaling the ranges
    x.domain([
        d3.min(entries, (c) => d3.min(c.values, (d) => d.date )),
        d3.max(entries, (c) => d3.max(c.values, (d) => d.date ))
    ]);

    y.domain([
        d3.min(entries, (c) => d3.min(c.values, (d) => d.value )),
        d3.max(entries, (c) => d3.max(c.values, (d) => d.value ))
    ]);

    let instrument = svg.selectAll('.instrument')
        .data(entries)
        .enter().append('g')
        .attr('class', 'instrument');

    // Adding the line
    instrument.append('path')
        .attr('class', (d, i) => `line line-${ i + 1 }`)
        .attr('d', (d) => line(d.values));

    entries.map((entry, i) => {
        entry.values.map((value) => {
            // Adding the dot
            svg.append('g')
                .attr('class', 'instrument')
                .append('circle')
                .attr('class', (d) => `circle circle-${ i + 1 }`)
                .attr('cx', x(value.date) + margin.left)
                .attr('cy', y(value.value) + margin.top)
                .attr('r', 4)
                .on('mouseover', function (d) {
                    showData(this, entry.id, value, i + 1, zoom);
                })
                .on('mouseout', function () {
                    hideData();
                });
        })
    });

    // Format and add grid on X-Axis
    gridX.attr('class', 'grid')
        .attr('transform', 'translate(' + margin.left + ',' + (y(0) + margin.top) + ')')
        .call(gridlinesX(x)
            .tickSize(-height)
            .tickFormat('')
        );

    // Format and add grid on Y-Axis
    gridY.attr('class', 'grid')
        .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
        .call(gridlinesY(y)
            .tickSize(-width)
            .tickFormat('')
        );

    // Add X-Axis
    xAxis.attr('class', 'axis axis-x')
        .attr('transform', 'translate(' + margin.left + ',' + (y(0) + margin.top) + ')')
        .call(d3.axisBottom(x));

    // Add Y-Axis
        yAxis.attr('class', 'axis axis-y')
        .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
        .call(d3.axisLeft(y));
};

// gridlines in x axis function
const gridlinesX = (x) => d3.axisBottom(x);

// gridlines in y axis function
const gridlinesY = (y) => d3.axisLeft(y);

function showData(obj, ins, d, index, zoom) {
    let format = d3.timeFormat(timeFormat(zoom));
    let coords = d3.mouse(obj);
    let infobox = document.getElementById('sq-infobox');
    let offsetX = coords[0];
    let offsetY = coords[1];
    // If left offset will be a value higher than 75% screen width, we move the tooltip to the left of the cursor
    if(offsetX > screenWidth() * 0.75) {
        offsetX -= 60;
    }
    // If top offset will be too close to the top edge, we move the tooltip to the top of the cursor
    if(offsetY < 120) {
        offsetY += 130;
    }
    infobox.style.left = (offsetX) + 'px';
    infobox.style.top = (offsetY) + 'px';
    infobox.style.display = 'block';
    infobox.innerHTML = `
        <h5 class="cat-${ index }">Instrument ${ ins }</h5>
        <div class="flexbox">
            <span>${ format(d.date) }</span>
            <strong>${ d.value.toFixed(2) }</strong>
        </div>`;
}

function hideData() {
    let infobox = document.getElementById('sq-infobox');
    infobox.style.display = 'none';
}

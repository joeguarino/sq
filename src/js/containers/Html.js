import React from 'react';

const Html = (props) => {
    return (
        <html lang="en">
        <head>
            <title>swissQuant [Guarino]</title>
            <meta charSet="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
            <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
            <meta httpEquiv="X-UA-Compatible" content="IE=edge,chrome=1" />
            <meta name="description" content="swissQuant - [Guarino] Programming Challenge"/>
            <link rel="stylesheet" type="text/css" href="/public/css/app.min.css"/>
        </head>
        <body className={ props.bodyClass }>
            <section id="sq-app">{props.children}</section>
            <script type="text/javascript" src="/public/js/app.min.js" defer />
        </body>
        </html>
    );
};

export default Html;
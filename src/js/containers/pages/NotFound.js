'use strict';

import React, { Component } from 'react';

class NotFound extends Component {

    componentDidMount(){
        document.body.classList.add('not-found');
    }

    componentWillUnmount(){
        document.body.classList.remove('not-found');
    }

    render(){
        return (
            <h1>
                <span>404</span>
                <span>Page not found</span>
            </h1>
        );
    }
}

export default NotFound;
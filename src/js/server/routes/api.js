'use strict';

import express from 'express';
import json from '../../../json/mktdata.json';

const router = express.Router();

router.get('/api', (req, res) => {
    res.send(json);
});

export default router;
'use strict';

import React, { Component } from 'react';
import Graph from '../components/Graph';

class Home extends Component {
    constructor(props){
        super(props);
    }

    componentDidMount(){
        document.body.classList.add('home');
    }

    componentWillUnmount(){
        document.body.classList.remove('home');
    }

    render(){
        return <Graph />;
    }
}

export default Home;
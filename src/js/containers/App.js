'use strict';

import React, { Component } from 'react';
import Header from './components/Header';
import Home from './pages/Home';
import NotFound from './pages/NotFound';

class App extends Component {
    render(){
        let content = this.props.path === '/' ? <Home /> : <NotFound />;

        return [
            <Header key="header" />,
            <main key="main">{ content }</main>
        ]
    }
}

export default App;
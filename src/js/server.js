'use strict';

import app from './server/routes';

const port = 3000;

let httpServer = require('http').createServer(app);

httpServer.listen(port, (err) => {
    if (err) {
        console.error("Error while launching the server", err);
    }
    else {
        console.log(`Server running on http://localhost:${port}\n`);
    }
});

export default httpServer;
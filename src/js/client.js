'use strict';

require('./../scss/app.scss');
require('es6-promise').polyfill();

import React from 'react';
import { hydrate } from 'react-dom';
import App from './containers/App';

function init() {
    window.removeEventListener('load', init);
    hydrate(
        <App path={ window.location.pathname } />,
        document.getElementById('sq-app')
    );
}

window.addEventListener('load', init);
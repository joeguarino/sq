'use strict';

import express from 'express';
import api from './routes/api';
import main from './routes/main';
import folders from './routes/folders';

const router = express();
router.use(api);
router.use(folders);
router.use(main);

export default router;
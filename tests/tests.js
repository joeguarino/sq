'use strict';

console.log('The following tests are based on the Mocha testing framework along with the Chai assertion library.');

import chai from 'chai';
import chaiHttp from 'chai-http';
import server from '../src/js/server';
import { formatData, formatEntry } from '../src/js/helpers/d3';

chai.use(chaiHttp);

const should = chai.should();
let promises = [];

describe('API', function() {
    describe('call()', function() {
        promises.push(new Promise((resolve) =>{
            it("Returns status 200", function(done) {
                chai.request(server)
                    .get('/api')
                    .end(function (err, res) {
                        res.should.have.status(200);
                        resolve();
                        done();
                    });
            });
        }));

        promises.push(new Promise((resolve) => {
            it("Returns a valid JSON", function (done) {
                chai.request(server)
                    .get('/api')
                    .end(function (err, res) {
                        res.should.have.status(200);
                        res.should.be.json;
                        resolve();
                        done();
                    });
            });
        }));

        promises.push(new Promise((resolve) => {
            it("Returns an Object as root", function (done) {
                chai.request(server)
                    .get('/api')
                    .end(function (err, res) {
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.a('object');
                        resolve();
                        done();
                    });
            });
        }));

        promises.push(new Promise((resolve) => {
            it("Returns an Object with item 'mktData' of type Array (not empty)", function (done) {
                chai.request(server)
                    .get('/api')
                    .end(function (err, res) {
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.a('object');
                        res.body.should.have.property('mktData');
                        res.body.mktData.should.be.a('array');
                        res.body.mktData.should.not.be.empty;
                        resolve();
                        done();
                    });
            });
        }));
    });
});

describe('Server', function() {
    describe('GET /', function() {
        promises.push(new Promise((resolve) => {
            it("Returns status 200", function (done) {
                chai.request(server)
                    .get('/')
                    .end(function (err, res) {
                        res.should.have.status(200);
                        resolve();
                        done();
                    });
            });
        }));
    });

    describe('GET /other-route', function() {
        promises.push(new Promise((resolve) => {
            it("Returns status 404", function (done) {
                chai.request(server)
                    .get('/other-route')
                    .end(function (err, res) {
                        res.should.have.status(404);
                        resolve();
                        done();
                    });
            });
        }));
    });
});

describe('Data processing', function() {
    describe('formatData()', function() {
        promises.push(new Promise((resolve) => {
            it("Returns an Array", function (done) {
                chai.request(server)
                    .get('/api')
                    .end(function (err, res) {
                        let entries = formatData(res.body.mktData);
                        entries.should.be.a('array');
                        resolve();
                        done();
                    });
            });
        }));
        promises.push(new Promise((resolve) => {
            it("Each array item is an Object with id (Number) and values (Array)", function (done) {
                chai.request(server)
                    .get('/api')
                    .end(function (err, res) {
                        let entries = formatData(res.body.mktData);
                        entries.map((entry) => {
                            entry.should.be.a('object');
                            entry.should.have.property('id');
                            entry.id.should.not.be.NaN;
                            entry.should.have.property('values');
                            entry.values.should.be.a('array');
                        });
                        resolve();
                        done();
                    });
            });
        }));
    });

    describe('formatEntry()', function(){
        promises.push(new Promise((resolve) => {
            it("Returns an Object with date (Date) and value (Number)", function (done) {
                chai.request(server)
                    .get('/api')
                    .end(function (err, res) {
                        let firstEntry = res.body.mktData[0];
                        let values = formatEntry(firstEntry.timeSeries.entries);
                        values.map((v) => {
                            v.should.be.a('object');
                            v.should.have.property('date');
                            validDate(v.date).should.be.true;
                            v.should.have.property('value');
                            v.value.should.not.be.NaN;
                        });
                        resolve();
                        done();
                    });
            });
        }));
    });
});

Promise.all(promises).then(() => {
    console.log('\nTests completed, shutting down server');
    server.close();
});

function validDate(d){
    if (Object.prototype.toString.call(d) === "[object Date]" ) {
        if (!isNaN(d.getTime())) {
            return true;
        }
    }
    return false;
}
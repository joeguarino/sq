'use strict';

import React, { Component } from 'react';
import Slider from 'rc-slider';

class Zoom extends Component {
    constructor(props){
        super(props);
        this.min = 0;
        this.max = 2;
        this.state = {
            value: this.min
        };
        this.onChange = this.onChange.bind(this);
        this.onAfterChange = this.onAfterChange.bind(this);
    }

    onChange(e){
        this.setState({
            value: e
        });
    }

    onAfterChange(e){
        if(typeof this.props.afterChange === 'function'){
            this.props.afterChange(e);
        }
    }

    render(){
        return (
            <section id="sq-zoom">
                <h5>Zoom <span>{ this.state.value + 1 }x</span></h5>
                <Slider className="slider" min={ this.min } max={ this.max } value={ this.state.value } step={ 1 } onChange={ this.onChange } onAfterChange={ this.onAfterChange } withBars />
            </section>
        )
    }
}

export default Zoom;
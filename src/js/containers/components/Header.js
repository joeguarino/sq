'use strict';

import React, { Component } from 'react';

class Header extends Component {
    render(){
        return (
            <header role="banner">
                <a href="/" title="DataGraph" className="logo">
                    <span className="screen-reader">DataGraph</span>
                </a>
            </header>
        )
    }
}

export default Header;
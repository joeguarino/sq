'use strict';

import React, { Component } from 'react';
import Zoom from './Zoom';
import Multiplier from './Multiplier';
import { buildGraph } from '../../helpers/d3';

class Graph extends Component {
    constructor(props){
        super(props);
        this.instruments = [];
        this.timeout = null;
        this.svgNode = '#sq-graph svg';
        this.zoom = 0;
        this.multiplier = 0;
        this.attachListeners = this.attachListeners.bind(this);
        this.postResize = this.postResize.bind(this);
        this.setZoom = this.setZoom.bind(this);
        this.setMultiplier = this.setMultiplier.bind(this);
        this.redraw = this.redraw.bind(this);
    }

    attachListeners(){
        window.addEventListener('resize', this.postResize);
    }

    postResize(){
        // This function avoids to be continuously triggered a re-draw by setting a timeout
        if(!!this.timeout){
            clearTimeout(this.timeout);
        }

        this.timeout = setTimeout(() => {
            this.redraw();
        }, 200);
    }

    setZoom(zoom){
        this.zoom = zoom;
        this.redraw();
    }

    setMultiplier(multiplier){
        this.multiplier = multiplier;
        this.redraw();
    }

    redraw(){
        document.querySelector(this.svgNode).innerHTML = '';
        buildGraph(this.svgNode, this.instruments, this.zoom, this.multiplier);
    }

    componentDidMount(){
        fetch('/api')
            .then((response) => response.json())
            .then((json) => {
                this.instruments = json.mktData;
                this.attachListeners();
                buildGraph(this.svgNode, this.instruments, this.zoom);
            })
            .catch((e) => {
                console.error('Error while parsing and displaying data', e);
            });
    }

    render(){
        return (
            <section id="sq-graph">
                <section id="sq-controls">
                    <Zoom afterChange={ this.setZoom } />
                    <Multiplier afterChange={ this.setMultiplier } />
                </section>
                <svg />
                <span id="sq-infobox" />
            </section>
        );
    }
}

export default Graph;
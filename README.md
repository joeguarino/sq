# swissQuant Programming Challenge
| Giovanni Guarino (giovanniguarino@me.com)

## Description
The project is written in Node.js and ReactJS using ES6 features and Babel + webpack to transpile. It also relies on polyfills to ensure backwards compatibility.

## Installation
To run everything at once just run `npm start` which will automatically (and subsequently):
1. install all the dependencies
2. build the .js and .css bundle
3. start the server.

Otherwise if you wish to launch the scripts separately you could use:
- `npm run server`
- `npm run client:build`

**Note:** Everything has been developed and tested using **node 6.9.1** and **npm 3.10.8**.

## Tests
Tests have been developed using **mocha** and **chai** and focus on 3 main areas:
- API
- Router
- Data processing

To launch the tests script simply run `npm run test`.

## Server
The webserver is based on Express.js and it serves 2 routes:
- /
- /api

Everything else will produce a 404.